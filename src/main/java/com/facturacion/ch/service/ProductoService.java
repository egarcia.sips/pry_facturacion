package com.facturacion.ch.service;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import com.facturacion.ch.entity.Producto;
import com.facturacion.ch.model.ProductoModel;
import com.facturacion.ch.repository.ProductoRepository;
import com.facturacion.ch.respuesta.ProductoRespuesta;



@Component
public class ProductoService {

	@Autowired
	private ProductoRepository productoRepository;
	@Autowired
	public ProductoService(ProductoRepository productoRepository) {
		this.productoRepository = productoRepository;
	}
	
	public Producto crearProducto(Producto producto) {
		return productoRepository.save(producto);
	}
	
	public Optional<Producto> buscarProductoById(Long idProducto){
		return productoRepository.findById(idProducto);
	}
	
	public ProductoRespuesta findAllModel() {
		ProductoRespuesta respuesta = new ProductoRespuesta();
		try {
			List<Producto> productos = productoRepository.findAll();
			List<ProductoModel> productoModel = new ArrayList<>();
			if (productos != null && productos.size() >0) {
				for (Producto p : productos) {
					productoModel.add(crearModelo(p));
				}
				respuesta.setCodigo("1");
				respuesta.setMensaje("OK");
				respuesta.setLista(productoModel);
				
			}else {
				respuesta.setCodigo("0");
				respuesta.setMensaje("No existe informacion");
			}
		} catch (Exception e) {
			respuesta.setCodigo("-1");
			respuesta.setMensaje("ERROR"+" "+e.getMessage());
		}
		return respuesta;
	}
	
	public ProductoModel crearModelo(Producto producto) {
		ProductoModel modelo = new ProductoModel();
		if (String.valueOf(producto.getId()) !=  null ) {
			modelo.setId(producto.getId());
			}
		if (producto.getDescripcion() != null) {
			modelo.setDescripcion(producto.getDescripcion());
		}
		if (producto.getPresentacion() != null) {
			modelo.setPresentacion(producto.getPresentacion());
		}
		if (String.valueOf(producto.getPreciocompra()) != null) {
			modelo.setPreciocompra(producto.getPreciocompra());
		} 
		if (producto.getProveedor() != null) {
			modelo.setProveedor(producto.getProveedor());
		}
		if (String.valueOf(producto.getStock()) != null) {
			modelo.setStock(producto.getStock());
		}
		
		return modelo;
	}
}
