package com.facturacion.ch.service;

import java.text.SimpleDateFormat;
import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.facturacion.ch.entity.Factura;
import com.facturacion.ch.model.ComprobanteModel;
import com.facturacion.ch.model.DetalleFacturaModel;
import com.facturacion.ch.model.FacturaModel;
import com.facturacion.ch.repository.FacturaRepository;
import com.facturacion.ch.respuesta.ComprobanteRespuesta;
import com.facturacion.ch.respuesta.FacturaRespuesta;


@Component
public class FacturaService {
	@Autowired
	private FacturaRepository facturaRepository;


	@Autowired
	public FacturaService(FacturaRepository facturaRepository) {
		this.facturaRepository = facturaRepository;
	}
	

	public Factura crearFactura(Factura factura) {
		return facturaRepository.save(factura);
	}
	
	public Optional<Factura> buscarFacturaByClientId(Long idCliente){
		return facturaRepository.findById(idCliente);
	}
	
	
	public FacturaRespuesta findAllModel() {
		FacturaRespuesta respuesta = new FacturaRespuesta();
		try {
			List<Factura> facturas = facturaRepository.findAll();
			List<FacturaModel> facturaModel = new ArrayList<>();
			if (facturas != null && facturas.size() >0) {
				for (Factura f : facturas) {
					facturaModel.add(crearModelo(f));
				}
				respuesta.setCodigo("1");
				respuesta.setMensaje("OK");
				respuesta.setLista(facturaModel);
				
			}else {
				respuesta.setCodigo("0");
				respuesta.setMensaje("No existe informacion");
			}
		} catch (Exception e) {
			respuesta.setCodigo("-1");
			respuesta.setMensaje("ERROR"+" "+e.getMessage());
		}
		return respuesta;
	}
	
	
	public FacturaModel crearModelo(Factura factura) {
		FacturaModel modelo = new FacturaModel();
		
		if (String.valueOf(factura.getId())!= null) {
			modelo.setId(factura.getId());
		}
		if (String.valueOf(factura.getCliente_id()) != null) {
			modelo.setCliente_id(factura.getCliente_id());
		}
		if (String.valueOf(factura.getSubtotal())!= null) {
			modelo.setSubtotal(factura.getSubtotal());
		}
		if (String.valueOf(factura.getTotalpagar())!= null) {
			modelo.setTotalpagar(factura.getTotalpagar());
		}
		if (String.valueOf(factura.getIva())!= null) {
			modelo.setIva(factura.getIva());
		}
//		if (factura.getFechafactura() != null) {
//			DateTimeFormatter fmt = DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm:ss.S");
//			System.out.println(factura.getFechafactura().toString());
//			LocalDate fechaFac = LocalDate.parse(factura.getFechafactura().toString(), fmt);
//			LocalDate ahora = LocalDate.now();
//			modelo.setFechafactura(factura.getFechafactura());
//		}
		if (String.valueOf(factura.getEstado()) != null) {
			modelo.setEstado(factura.getEstado());
		}
		
		return modelo;
	}
	
	public FacturaRespuesta grabar(FacturaModel entrada) {
		FacturaRespuesta respuesta = new FacturaRespuesta();
		try {
			respuesta.setCliente(crearModelo(facturaRepository.saveAndFlush(crearObjeto(entrada))));
			respuesta.setLista(null);
		} catch (Exception e) {
			respuesta.setCliente(null);
			respuesta.setLista(null);
			System.out.println(e);
		}
		return respuesta;
	}
	
	public Factura crearObjeto (FacturaModel factura) {
		Factura respuesta = new Factura();
//		if (factura.getId()> 0 ) {
//			respuesta.setId(factura.getId());
//		}
		if (factura.getCliente_id()> 0 ) {
			respuesta.setCliente_id(factura.getCliente_id());
		}
		if (factura.getEstado()> 0 ) {
			respuesta.setEstado(factura.getEstado());
		}
		if (factura.getIva()> 0 ) {
			respuesta.setIva(factura.getIva());
		}
		if (factura.getSubtotal()> 0 ) {
			respuesta.setSubtotal(factura.getSubtotal());
		}
		if (factura.getTotalpagar()> 0 ) {
			respuesta.setTotalpagar(factura.getTotalpagar());
		}
		
		return respuesta;
	}
	
}
	
