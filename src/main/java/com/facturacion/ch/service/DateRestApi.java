package com.facturacion.ch.service;




import java.util.List;

import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;

@Service
public class DateRestApi {
	public EstructuraApiExterna getFecha() {
        RestTemplate restTemplate = new RestTemplate();
        final String url = "http://worldclockapi.com/api/json/utc/now";
        ResponseEntity<EstructuraApiExterna> resp= 
        		restTemplate.getForEntity(url, EstructuraApiExterna.class);
        
        return resp.getBody();
    }
}
