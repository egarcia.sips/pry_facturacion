package com.facturacion.ch.service;

import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Locale;
import java.util.Optional;
import org.apache.el.parser.ParseException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.web.client.RestTemplate;

import com.facturacion.ch.entity.DetalleFactura;
import com.facturacion.ch.entity.Factura;
import com.facturacion.ch.entity.Producto;
import com.facturacion.ch.entity.Cliente;
import com.facturacion.ch.model.ComprobanteModel;
import com.facturacion.ch.model.DetalleFacturaModel;
import com.facturacion.ch.model.FacturaModel;
import com.facturacion.ch.repository.ClienteRepository;
import com.facturacion.ch.repository.DetalleFacturaRepository;
import com.facturacion.ch.repository.FacturaRepository;
import com.facturacion.ch.repository.ProductoRepository;
import com.facturacion.ch.respuesta.ComprobanteRespuesta;
import com.facturacion.ch.respuesta.DetalleFacturaRespuesta;
import jakarta.persistence.Column;
import jakarta.persistence.JoinColumn;
import jakarta.persistence.ManyToOne;

@Component
public class DetalleFacturaService {
	@Autowired
	private DetalleFacturaRepository detalleFacturaRepository;
	@Autowired
	private ClienteRepository clienteRepository;
	@Autowired
	private FacturaRepository facturaRepository;
	@Autowired
	private ProductoRepository productoRepository;
	@Autowired
	private DateRestApi dateRestApi;


	@Autowired
	public DetalleFacturaService(FacturaRepository facturaRepository, DetalleFacturaRepository detalleFacturaRepository,
			ClienteRepository clienteRepository,ProductoRepository productoRepository) {
		this.detalleFacturaRepository = detalleFacturaRepository;
		this.clienteRepository = clienteRepository;
		this.facturaRepository = facturaRepository;
		this.productoRepository = productoRepository;
	}

	public DetalleFactura crearDetalleFactura(DetalleFactura detalleProducto) {
		return detalleFacturaRepository.save(detalleProducto);
	}

	public Optional<DetalleFactura> buscarDetalleFacturaById(Long idDetalleFactura) {
		return detalleFacturaRepository.findById(idDetalleFactura);
	}

	public <S extends Factura> S saveAndFlush(S entity) {
		return facturaRepository.saveAndFlush(entity);
	}

	public <S extends DetalleFactura> S saveAndFlush(S entity) {
		return detalleFacturaRepository.saveAndFlush(entity);
	}

	public DetalleFacturaRespuesta findAllModel() {
		DetalleFacturaRespuesta respuesta = new DetalleFacturaRespuesta();
		try {
			List<DetalleFactura> detallesfactura = detalleFacturaRepository.findAll();
			List<DetalleFacturaModel> detalleFacturaModel = new ArrayList<>();
			if (detallesfactura != null && detallesfactura.size() > 0) {
				for (DetalleFactura df : detallesfactura) {
					detalleFacturaModel.add(crearModelo(df));
				}
				respuesta.setCodigo("1");
				respuesta.setMensaje("OK");
				respuesta.setLista(detalleFacturaModel);

			} else {
				respuesta.setCodigo("0");
				respuesta.setMensaje("No existe informacion");
			}
		} catch (Exception e) {
			respuesta.setCodigo("-1");
			respuesta.setMensaje("ERROR" + " " + e.getMessage());
		}
		return respuesta;
	}

	public DetalleFacturaModel crearModelo(DetalleFactura df) {
		DetalleFacturaModel modelo = new DetalleFacturaModel();

		if (String.valueOf(df.getId()) != null) {
			modelo.setId(df.getId());
		}
		if (String.valueOf(df.getFactura_id()) != null) {
			modelo.setFactura_id(df.getFactura_id());
		}
		if (String.valueOf(df.getProducto_id()) != null) {
			modelo.setProducto_id(df.getProducto_id());
		}
		if (String.valueOf(df.getCantidad()) != null) {
			modelo.setCantidad(df.getCantidad());
		}
		if (String.valueOf(df.getPreciounitario()) != null) {
			modelo.setPreciounitario(df.getPreciounitario());
		}
		if (String.valueOf(df.getDescuento()) != null) {
			modelo.setDescuento(df.getDescuento());
		}
		if (String.valueOf(df.getPreciototal()) != null) {
			modelo.setPreciototal(df.getPreciototal());
		}
		if (String.valueOf(df.getEstado()) != null) {
			modelo.setEstado(df.getEstado());
		}
		return modelo;
	}

	public ComprobanteRespuesta registro (ComprobanteModel entrada) {
		ComprobanteRespuesta respuesta = new ComprobanteRespuesta();
		LocalDateTime datetime = LocalDateTime.now();
		List<DetalleFacturaModel> lineas = new ArrayList<>();
		List<DetalleFacturaModel> lineas2 = new ArrayList<>();
		Optional<Producto> productos;
		Producto stockProductos = new Producto();
		Long clienteId = Long.valueOf(entrada.getCliente().getCliente_id());
		Optional<Cliente> cliente = clienteRepository.findById(clienteId);
		int contadorProductos=0;
		int totalVenta=0;
		String fechaActual;
		boolean stock = true;
		
		if (cliente.isPresent()){ //Verifica si el cliente existe
		
			for (int i =0; i < entrada.getLista().size();i++) {
				long productoId = Long.valueOf(entrada.getLista().get(i).getProducto_id());
				productos = productoRepository.findById(productoId);
				
				if (productos.isPresent() ) {
					contadorProductos++;
					stockProductos = productoRepository.getOne(productoId);
					totalVenta= totalVenta+entrada.getLista().get(i).getCantidad();
					
				}
				
				if (stockProductos.getStock() > 0) {
					System.out.println("Cantidad producto "+stockProductos.getDescripcion()+" vendido : "+entrada.getLista().get(i).getCantidad());
					System.out.println("Stock existente : "+stockProductos.getStock());
					if (stockProductos.getStock() < entrada.getLista().get(i).getCantidad()) {
						stock = false;
					}
				}
			}System.out.println("Total cantidad venta: "+ totalVenta);
			if (contadorProductos == entrada.getLista().size()) {//verifica si todos los productos existen o no
				System.out.println("Total de productos vendidos"+contadorProductos);//Imprime la cantidad de productos vendidos
				if (stock == true) {// verifica si hay stock para generar el comprobante
					try {
						respuesta.setCodigo("1");
						respuesta.setMensaje("COMPROBANTE REGISTRADO EXITOSAMENTE");
						fechaActual = dateRestApi.getFecha().getCurrentDateTime();//Obtiene la fecha de la URL externa
						if(fechaActual != null) {
							respuesta.setFecha(fechaActual);
						}else {
							respuesta.setFecha(datetime.toString());
						}
						
						respuesta.setCliente(crearModeloFactura(saveAndFlush(crearObjetoFactura(entrada.getCliente()))));
						for (DetalleFacturaModel detalleFacturaModel :entrada.getLista() ) {
							lineas.add(detalleFacturaModel);
						}
						if (lineas.size()>0) {
							for (int i=0; i< lineas.size(); i++) {
								lineas2.add(crearModeloDetalleFactura(saveAndFlush(crearObjetoDetalleFactura(entrada.getLista().get(i)))));
								
							}
							respuesta.setLineas(lineas2);
					    }
					} catch (Exception e) {
						System.out.println(e);
						respuesta.setCodigo("0");
						respuesta.setMensaje("NO SE PUDO REGISTRAR EL COMPROBANTE");
					}
					return respuesta;
				}else{
					respuesta.setCodigo("0");
					respuesta.setMensaje("UNO DE LOS PRODUCTOS NO TIENE STOCK SUFICIENTE");
					return respuesta;
				}
		    }else {
		    	respuesta.setCodigo("0");
				respuesta.setMensaje("UNO DE LOS PRODUCTOS NO EXISTEN");
				return respuesta;	
		    }
		}else {
			respuesta.setCodigo("0");
			respuesta.setMensaje("CLIENTE NO SE ENCUENTRA REGISTRADO");
			return respuesta;
		}	
		
		
	}

	public DetalleFacturaModel crearModeloDetalleFactura(DetalleFactura detalleFactura) {
		DetalleFacturaModel respuesta = new DetalleFacturaModel();
		if (String.valueOf(detalleFactura.getId()) != null) {
			respuesta.setId(detalleFactura.getId());
		}
		if (String.valueOf(detalleFactura.getProducto_id()) != null) {
			respuesta.setProducto_id(detalleFactura.getProducto_id());
		}
		if (String.valueOf(detalleFactura.getCantidad()) != null) {
			respuesta.setCantidad(detalleFactura.getCantidad());
		}
		if (String.valueOf(detalleFactura.getPreciounitario()) != null) {
			respuesta.setPreciounitario(detalleFactura.getPreciounitario());
		}
		if (String.valueOf(detalleFactura.getDescuento()) != null) {
			respuesta.setDescuento(detalleFactura.getDescuento());
		}
		if (String.valueOf(detalleFactura.getPreciototal()) != null) {
			respuesta.setPreciototal(detalleFactura.getPreciototal());
		}
		if (String.valueOf(detalleFactura.getEstado()) != null) {
			respuesta.setEstado(detalleFactura.getEstado());
		}

		return respuesta;
	}

	public Factura crearObjetoFactura(FacturaModel factura) {
		Factura respuesta = new Factura();
		if (factura.getCliente_id() > 0) {
			respuesta.setCliente_id(factura.getCliente_id());
		}
		if (factura.getSubtotal() > 0) {
			respuesta.setSubtotal(factura.getSubtotal());
		}
		if (factura.getTotalpagar() > 0) {
			respuesta.setTotalpagar(factura.getTotalpagar());
		}
		if (factura.getIva() > 0) {
			respuesta.setIva(factura.getIva());
		}
		if (factura.getEstado() > 0) {
			respuesta.setEstado(factura.getEstado());
		}
		return respuesta;
	}

	public FacturaModel crearModeloFactura(Factura factura) {
		FacturaModel modelo = new FacturaModel();

		if (String.valueOf(factura.getId()) != null) {
			modelo.setId(factura.getId());
		}
		if (String.valueOf(factura.getCliente_id()) != null) {
			modelo.setCliente_id(factura.getCliente_id());
		}
		if (String.valueOf(factura.getSubtotal()) != null) {
			modelo.setSubtotal(factura.getSubtotal());
		}
		if (String.valueOf(factura.getTotalpagar()) != null) {
			modelo.setTotalpagar(factura.getTotalpagar());
		}
		if (String.valueOf(factura.getIva()) != null) {
			modelo.setIva(factura.getIva());
		}
		if (String.valueOf(factura.getEstado()) != null) {
			modelo.setEstado(factura.getEstado());
		}
		return modelo;
	}

	public DetalleFactura crearObjetoDetalleFactura(DetalleFacturaModel detalleFactura) {
		DetalleFactura respuesta = new DetalleFactura();

		if (detalleFactura.getProducto_id() > 0) {
			respuesta.setProducto_id(detalleFactura.getProducto_id());
		}
		if (detalleFactura.getCantidad() > 0) {
			respuesta.setCantidad(detalleFactura.getCantidad());
		}
		if (detalleFactura.getPreciounitario() > 0) {
			respuesta.setPreciounitario(detalleFactura.getPreciounitario());
		}
		if (detalleFactura.getDescuento() > 0) {
			respuesta.setDescuento(detalleFactura.getDescuento());
		}
		if (detalleFactura.getPreciototal() > 0) {
			respuesta.setPreciototal(detalleFactura.getPreciototal());
		}
		if (detalleFactura.getEstado() > 0) {
			respuesta.setEstado(detalleFactura.getEstado());
		}

		return respuesta;
	}
	
	
	
}
