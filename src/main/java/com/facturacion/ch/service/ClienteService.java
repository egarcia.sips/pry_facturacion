package com.facturacion.ch.service;


import java.util.ArrayList;
import java.util.List;
import java.util.Optional;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.facturacion.ch.entity.Cliente;
import com.facturacion.ch.model.ClienteModel;
import com.facturacion.ch.repository.ClienteRepository;
import com.facturacion.ch.respuesta.ClienteRespuesta;


@Component
public class ClienteService {
	
	@Autowired
	private ClienteRepository clienteRepository;
	
	@Autowired
	public ClienteService(ClienteRepository clienteRepository) {
		this.clienteRepository = clienteRepository;
	}
	
	
	public Cliente crearCliente(Cliente cliente) {
		return clienteRepository.save(cliente);
	}
	
	public Optional<Cliente> buscarClienteById(Long idCliente){
		return clienteRepository.findById(idCliente);
	}
	
	
	/*Servicio que devuelve una respuesta personalizada y trasnforma miobjeto de tipo cliente de Base de datos a un modelo personalizado*/
	public ClienteRespuesta findAllModel() {
		ClienteRespuesta respuesta = new ClienteRespuesta();
		try {
			List<Cliente> clientes = clienteRepository.findAll();
			List<ClienteModel> clienteModel = new ArrayList<>();
			if (clientes != null && clientes.size() >0) {
				for (Cliente c : clientes) {
					clienteModel.add(crearModelo(c));
				}
				respuesta.setCodigo("1");
				respuesta.setMensaje("OK");
				respuesta.setLista(clienteModel);
				
			}else {
				respuesta.setCodigo("0");
				respuesta.setMensaje("No existe informacion");
			}
		} catch (Exception e) {
			respuesta.setCodigo("-1");
			respuesta.setMensaje("ERROR"+" "+e.getMessage());
		}
		return respuesta;
	} 
	/*Transforma mi objeto de BDD a mi modelo personalizado y aqui puedo manipular los datos a mi antojo para entregar
	 * hacia la capa superio y principalmente al endpoint l respuesta que necesita
	 * dejo en consola las edades para saber si esta transformando bien*/
	public ClienteModel crearModelo(Cliente cliente) {
		ClienteModel modelo = new ClienteModel();
		if (String.valueOf(cliente.getId()) !=  null ) {
			modelo.setId(cliente.getId());
		}
		if (cliente.getNombres() != null) {
			modelo.setNombres(cliente.getNombres());
		}
		if (cliente.getApellidos() != null) {
			modelo.setApellidos(cliente.getApellidos());
		}
		if (cliente.getRazonsocial() != null) {
			modelo.setRazonsocial(cliente.getRazonsocial());
		}
		if (cliente.getCorreo() != null) {
			modelo.setCorreo(cliente.getCorreo());
		}
		if (cliente.getTelefono() != null) {
			modelo.setTelefono(cliente.getTelefono());
		}
		if (cliente.getIdentificacion() != null) {
			modelo.setIdentificacion(cliente.getIdentificacion());
		}
		if (String.valueOf(cliente.getTipoidentificacion()) != null) {
			modelo.setTipoidentificacion(cliente.getTipoidentificacion());
		}
	
//		if (cliente.getFechaNacimiento() != null) {
//			DateTimeFormatter fmt = DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm:ss.S");
//			System.out.println(cliente.getFechaNacimiento().toString());
//			LocalDate fechaNac = LocalDate.parse(cliente.getFechaNacimiento().toString(), fmt);
//			LocalDate ahora = LocalDate.now();
//			Period periodo = Period.between(fechaNac, ahora);
//			System.out.println("Tu edad es: %s años, %s meses y %s días " + periodo.getYears() +" "+ periodo.getMonths() +" "+ periodo.getDays());
//			modelo.setEdad(periodo.getYears()+ " años");
//		}
		
		return modelo;
	}
}
