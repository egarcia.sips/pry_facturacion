package com.facturacion.ch.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.data.querydsl.QuerydslPredicateExecutor;
import org.springframework.stereotype.Repository;

import com.facturacion.ch.entity.DetalleFactura;


@Repository
public interface DetalleFacturaRepository extends JpaRepository<DetalleFactura, Long>{

}
