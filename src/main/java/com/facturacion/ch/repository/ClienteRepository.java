package com.facturacion.ch.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.data.querydsl.QuerydslPredicateExecutor;

import com.facturacion.ch.entity.Cliente;

public interface ClienteRepository extends JpaRepository<Cliente, Long>{

}
