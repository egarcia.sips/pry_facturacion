package com.facturacion.ch.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import com.facturacion.ch.model.ComprobanteModel;
import com.facturacion.ch.respuesta.ComprobanteRespuesta;
import com.facturacion.ch.service.DetalleFacturaService;

@RestController
@RequestMapping("/comprobante")
public class ComprobanteController {

	@Autowired
	private DetalleFacturaService detalleFacturaService;
	
	@PostMapping("/registro")
    @ResponseBody
	public ComprobanteRespuesta registro(@RequestBody ComprobanteModel entrada) {
		return detalleFacturaService.registro(entrada);
	}
}
