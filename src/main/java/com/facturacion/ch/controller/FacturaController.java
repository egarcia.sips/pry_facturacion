package com.facturacion.ch.controller;

import java.net.URI;
import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;
import com.facturacion.ch.entity.Factura;
import com.facturacion.ch.model.ComprobanteModel;
import com.facturacion.ch.model.DetalleFacturaModel;
import com.facturacion.ch.model.FacturaModel;
import com.facturacion.ch.respuesta.ComprobanteRespuesta;
import com.facturacion.ch.respuesta.FacturaRespuesta;
import com.facturacion.ch.service.DetalleFacturaService;
import com.facturacion.ch.service.FacturaService;


@RestController
@RequestMapping("/factura")
public class FacturaController {
	@Autowired
	private FacturaService facturaService;

	
	@GetMapping("/findAll")
    @ResponseBody
	public FacturaRespuesta findAll() {
		return facturaService.findAllModel();
	}
	
	@GetMapping (value = "/{id}", produces = {MediaType.APPLICATION_JSON_VALUE})
	public ResponseEntity<?>getFacturaById(@PathVariable(name = "id")Long id){
		Optional<Factura> factura = facturaService.buscarFacturaByClientId(id);
		if (factura.isPresent()) {
			return ResponseEntity.ok(factura);
		}else {
			return ResponseEntity.notFound().build();
		}
		
	}
	
//	@PostMapping(consumes = {MediaType.APPLICATION_JSON_VALUE})
//	public ResponseEntity<?> guardarFactura (@RequestBody Factura factura){
//		try {
//			Factura facturaGuardado = facturaService.crearFactura(factura);
//			return ResponseEntity.created(URI.create("")).body(facturaGuardado);
//		}catch(Exception e){
//			e.printStackTrace();
//			return ResponseEntity.internalServerError().body(null);
//		}
//		
//	}
	
	@PostMapping("/grabar")
    @ResponseBody
	public FacturaRespuesta save(@RequestBody FacturaModel entrada) {
		return facturaService.grabar(entrada);
	}
}
