package com.facturacion.ch.controller;
import java.net.URI;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;
import com.facturacion.ch.entity.DetalleFactura;
import com.facturacion.ch.respuesta.DetalleFacturaRespuesta;
import com.facturacion.ch.service.DetalleFacturaService;


@RestController
@RequestMapping("/detalefactura")
public class DetalleFacturaController {
	@Autowired
	private DetalleFacturaService detalleFacturaService;
	
	
	@GetMapping("/findAll")
    @ResponseBody
	public DetalleFacturaRespuesta findAll() {
		return detalleFacturaService.findAllModel();
	}
	
	@GetMapping (value = "/{id}", produces = {MediaType.APPLICATION_JSON_VALUE})
	public ResponseEntity<?>getClienteById(@PathVariable(name = "id")Long id){
		Optional<DetalleFactura> detalleFactura = detalleFacturaService.buscarDetalleFacturaById(id);
		if (detalleFactura.isPresent()) {
			return ResponseEntity.ok(detalleFactura);
		}else {
			return ResponseEntity.notFound().build();
		}
		
	}
	
	@PostMapping(consumes = {MediaType.APPLICATION_JSON_VALUE})
	public ResponseEntity<?> guardarCliente (@RequestBody DetalleFactura detalleFactura){
		try {
			DetalleFactura detalleFacturaGuardado = detalleFacturaService.crearDetalleFactura(detalleFactura);
			return ResponseEntity.created(URI.create("")).body(detalleFacturaGuardado);
		}catch(Exception e){
			e.printStackTrace();
			return ResponseEntity.internalServerError().body(null);
		}
		
	}
}
