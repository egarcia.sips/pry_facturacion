package com.facturacion.ch.controller;

import java.net.URI;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;
import com.facturacion.ch.entity.Producto;
import com.facturacion.ch.respuesta.ProductoRespuesta;
import com.facturacion.ch.service.ProductoService;


@RestController
@RequestMapping("/producto")
public class ProductoController {

	@Autowired
	private ProductoService productoService;
	
	@GetMapping("/findAll")
    @ResponseBody
	public ProductoRespuesta findAll() {
		return productoService.findAllModel();
	}
	
	@GetMapping (value = "/{id}", produces = {MediaType.APPLICATION_JSON_VALUE})
	public ResponseEntity<?>getProductoById(@PathVariable(name = "id")Long id){
		Optional<Producto> producto = productoService.buscarProductoById(id);
		if (producto.isPresent()) {
			return ResponseEntity.ok(producto);
		}else {
			return ResponseEntity.notFound().build();
		}
		
	}
	
	@PostMapping(consumes = {MediaType.APPLICATION_JSON_VALUE})
	public ResponseEntity<?> guardarProducto (@RequestBody Producto producto){
		try {
			Producto productoGuardado = productoService.crearProducto(producto);
			return ResponseEntity.created(URI.create("")).body(productoGuardado);
		}catch(Exception e){
			e.printStackTrace();
			return ResponseEntity.internalServerError().body(null);
		}
		
	}
}
