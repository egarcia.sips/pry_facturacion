package com.facturacion.ch.controller;


import java.net.URI;
import java.util.Optional;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import com.facturacion.ch.entity.Cliente;
import com.facturacion.ch.respuesta.ClienteRespuesta;
import com.facturacion.ch.service.ClienteService;



@RestController
@RequestMapping("/cliente")
public class ClienteContrller {

	@Autowired
	private ClienteService clienteService;
	
	@GetMapping (value = "/{id}", produces = {MediaType.APPLICATION_JSON_VALUE})
	public ResponseEntity<?>getClienteById(@PathVariable(name = "id")Long id){
		Optional<Cliente> cliente = clienteService.buscarClienteById(id);
		if (cliente.isPresent()) {
			return ResponseEntity.ok(cliente);
		}else {
			return ResponseEntity.notFound().build();
		}
		
	}
	
	@PostMapping(consumes = {MediaType.APPLICATION_JSON_VALUE})
	public ResponseEntity<?> guardarCliente (@RequestBody Cliente cliente){
		try {
			Cliente clienteGuardado = clienteService.crearCliente(cliente);
			return ResponseEntity.created(URI.create("")).body(clienteGuardado);
		}catch(Exception e){
			e.printStackTrace();
			return ResponseEntity.internalServerError().body(null);
		}
		
	}
	/*Metodo que devuelve todos los clientes considerando que en vez de la fecha de nacimiento debe devolver la EDAD del cliente
	 *Este metodo devuelve una respuesta personalizada en base a un modeo propio dando la posibilidad de poder manipular todos los datos
	 *que se envian hacia la base de datos y viceversa*/
	@GetMapping("/findAll")
    @ResponseBody
	public ClienteRespuesta findAll() {
		return clienteService.findAllModel();
	}
}
