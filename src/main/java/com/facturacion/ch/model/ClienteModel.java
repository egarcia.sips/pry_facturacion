package com.facturacion.ch.model;

import java.io.Serializable;

public class ClienteModel implements Serializable{

	/**
	 * Esta clase tiene el objetivo de representar un modelo personalizado de la respuesta que retornara mi endpoint
	 * cuando sea invocado sin importar el tipo de dato que se tenga en la base de datos, para lo cual defini cuatro variables que se menciona 
	 * en el desafio y estas todas son String para que pueda manipular los datos a mi necesidad
	 * Esta clase sera la que se entiendo con mi capa de persistencia a la cual se le entragara un objeto de tipo CLIENTE
	 */
	private static final long serialVersionUID = 1L;
	
	
    private int id;
	private String nombres;
	private String apellidos;
	private String razonsocial;
	private String correo;
	private String telefono;
	private int tipoidentificacion;
	private String identificacion;
	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}
	public String getNombres() {
		return nombres;
	}
	public void setNombres(String nombres) {
		this.nombres = nombres;
	}
	public String getApellidos() {
		return apellidos;
	}
	public void setApellidos(String apellidos) {
		this.apellidos = apellidos;
	}
	public String getRazonsocial() {
		return razonsocial;
	}
	public void setRazonsocial(String razonsocial) {
		this.razonsocial = razonsocial;
	}
	public String getCorreo() {
		return correo;
	}
	public void setCorreo(String correo) {
		this.correo = correo;
	}
	public String getTelefono() {
		return telefono;
	}
	public void setTelefono(String telefono) {
		this.telefono = telefono;
	}
	public int getTipoidentificacion() {
		return tipoidentificacion;
	}
	public void setTipoidentificacion(int tipoidentificacion) {
		this.tipoidentificacion = tipoidentificacion;
	}
	public String getIdentificacion() {
		return identificacion;
	}
	public void setIdentificacion(String identificacion) {
		this.identificacion = identificacion;
	}
	
}
