package com.facturacion.ch.model;

import java.io.Serializable;
import java.util.List;

public class ComprobanteModel implements Serializable{

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

    private FacturaModel  cliente;

    private List<DetalleFacturaModel> lineas;

	public FacturaModel getCliente() {
		return cliente;
	}

	public void setCliente(FacturaModel cliente) {
		this.cliente = cliente;
	}

	public List<DetalleFacturaModel> getLista() {
		return lineas;
	}

	public void setLista(List<DetalleFacturaModel> lineas) {
		this.lineas = lineas;
	}
    
    
}
