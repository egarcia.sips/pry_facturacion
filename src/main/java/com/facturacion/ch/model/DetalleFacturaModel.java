package com.facturacion.ch.model;

import java.io.Serializable;

public class DetalleFacturaModel  implements Serializable{

	
    /**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	
	
	private int id;
	private int factura_id;
	private int producto_id;
	private int cantidad;
	private Double preciounitario;
	private Double descuento;
	private Double preciototal;
	private Double estado;
	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}
	public int getFactura_id() {
		return factura_id;
	}
	public void setFactura_id(int factura_id) {
		this.factura_id = factura_id;
	}
	public int getProducto_id() {
		return producto_id;
	}
	public void setProducto_id(int producto_id) {
		this.producto_id = producto_id;
	}
	public int getCantidad() {
		return cantidad;
	}
	public void setCantidad(int cantidad) {
		this.cantidad = cantidad;
	}
	public Double getPreciounitario() {
		return preciounitario;
	}
	public void setPreciounitario(Double preciounitario) {
		this.preciounitario = preciounitario;
	}
	public Double getDescuento() {
		return descuento;
	}
	public void setDescuento(Double descuento) {
		this.descuento = descuento;
	}
	public Double getPreciototal() {
		return preciototal;
	}
	public void setPreciototal(Double preciototal) {
		this.preciototal = preciototal;
	}
	public Double getEstado() {
		return estado;
	}
	public void setEstado(Double estado) {
		this.estado = estado;
	}
	
	
	
}
