package com.facturacion.ch.respuesta;

import java.io.Serializable;
import java.util.List;

import com.facturacion.ch.model.ClienteModel;


public class ClienteRespuesta implements Serializable{
	
	
	/**
	 * Esta calse simula mi respuesta personalizada cuando el endpoint sea invocado y el servicio necesite devolver una respuesta 
	 * personalizada, tiene como base el modelo creado (ClienteModel.java) esta clase sera la que se entienda con la capa superior.
	 * 
	 * El modelo creado es estandar y mostrra en el JSON de la respuesta una respuesta estandar, de tal forma, que si 
	 * la peticion es mostrar varios regitros, entonces devolvera una lista y no un cliente, pero si la peticion solicita un solo registro
	 * devolvera un solo modelo y no una lista, para cuyoscasos el valor sera NULL. Sera cuestion del cliente que sepa como 
	 * armar su cliente y como implementarlo en el FRONTEND
	 * 
	 */
	private static final long serialVersionUID = 1L;

	private String codigo; //almacenara el codigo de respuesta del servicio web Ejm 200, 400, 

    private String mensaje; // almacenara el mensaje asociado a la respuesta que entregue el servicio web Ejm OK, ERROR, 

    private ClienteModel  cliente;// almacenara el onjeto de tipo cliente en mi modelo personalizado

    private List<ClienteModel> lista; // almacenara una lista de objetos de mi modelo personalizado

	public String getCodigo() {
		return codigo;
	}

	public void setCodigo(String codigo) {
		this.codigo = codigo;
	}

	public String getMensaje() {
		return mensaje;
	}

	public void setMensaje(String mensaje) {
		this.mensaje = mensaje;
	}

	public ClienteModel getPaciente() {
		return cliente;
	}

	public void setPaciente(ClienteModel cliente) {
		this.cliente = cliente;
	}

	public List<ClienteModel> getLista() {
		return lista;
	}

	public void setLista(List<ClienteModel> lista) {
		this.lista = lista;
	}
    
    
}
