package com.facturacion.ch.respuesta;

import java.io.Serializable;
import java.util.List;
import com.facturacion.ch.model.DetalleFacturaModel;
import com.facturacion.ch.model.FacturaModel;

public class ComprobanteRespuesta implements Serializable{

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	private String codigo; 

    private String mensaje; 
    
    private String fecha;

    private FacturaModel  cliente;

    private List<DetalleFacturaModel> lineas;

    
    
	public String getFecha() {
		return fecha;
	}

	public void setFecha(String fecha) {
		this.fecha = fecha;
	}

	public String getCodigo() {
		return codigo;
	}

	public void setCodigo(String codigo) {
		this.codigo = codigo;
	}

	public String getMensaje() {
		return mensaje;
	}

	public void setMensaje(String mensaje) {
		this.mensaje = mensaje;
	}

	public FacturaModel getCliente() {
		return cliente;
	}

	public void setCliente(FacturaModel cliente) {
		this.cliente = cliente;
	}

	public List<DetalleFacturaModel> getLineas() {
		return lineas;
	}

	public void setLineas(List<DetalleFacturaModel> lineas) {
		this.lineas = lineas;
	}


    
    
}
