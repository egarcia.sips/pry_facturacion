package com.facturacion.ch.entity;

import java.util.Date;

import jakarta.persistence.CascadeType;
import jakarta.persistence.Column;
import jakarta.persistence.Entity;
import jakarta.persistence.FetchType;
import jakarta.persistence.GeneratedValue;
import jakarta.persistence.GenerationType;
import jakarta.persistence.Id;
import jakarta.persistence.JoinColumn;
import jakarta.persistence.ManyToOne;
import jakarta.persistence.OneToMany;
import jakarta.persistence.Table;

@Entity
@Table(name = "FACTURA")
public class Factura {
	
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private int id;
	
	//@ManyToOne(targetEntity = Cliente.class)
	@Column(name="CLIENTE_ID")
	private int cliente_id;
	
	@Column(name = "SUBTOTAL")
	private double subtotal;
	
	@Column(name = "TOTALPAGAR")
	private double totalpagar;
	
	@Column(name = "IVA")
	private double iva;
	
	
	@Column(name = "ESTADO")
	private int estado;

	public Factura(int id, int cliente_id, double subtotal, double totalpagar, double iva,
			int estado) {
		super();
		this.id = id;
		this.cliente_id = cliente_id;
		this.subtotal = subtotal;
		this.totalpagar = totalpagar;
		this.iva = iva;
		this.estado = estado;
	}

	public Factura() {
		super();
		// TODO Auto-generated constructor stub
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public int getCliente_id() {
		return cliente_id;
	}

	public void setCliente_id(int cliente_id) {
		this.cliente_id = cliente_id;
	}

	public double getSubtotal() {
		return subtotal;
	}

	public void setSubtotal(double subtotal) {
		this.subtotal = subtotal;
	}

	public double getTotalpagar() {
		return totalpagar;
	}

	public void setTotalpagar(double totalpagar) {
		this.totalpagar = totalpagar;
	}

	public double getIva() {
		return iva;
	}

	public void setIva(double iva) {
		this.iva = iva;
	}


	public int getEstado() {
		return estado;
	}

	public void setEstado(int estado) {
		this.estado = estado;
	}

	
	
	

}
