package com.facturacion.ch.entity;

import jakarta.persistence.Column;
import jakarta.persistence.Entity;
import jakarta.persistence.GeneratedValue;
import jakarta.persistence.GenerationType;
import jakarta.persistence.Id;
import jakarta.persistence.Table;

@Entity
@Table(name = "PRODUCTO")
public class Producto {
	
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private int id;
	
	@Column(name = "DESCRIPCION")
	private String descripcion;
	
	@Column(name = "PRESENTACION")
	private String presentacion;
	
	@Column(name = "PRECIOCOMPRA")
	private double preciocompra;
	
	@Column(name = "PROVEEDOR")
	private String proveedor;
	
	@Column(name = "STOCK")
	private double stock;

	

	public Producto(int id, String descripcion, String presentacion, double preciocompra, String proveedor,
			double stock) {
		super();
		this.id = id;
		this.descripcion = descripcion;
		this.presentacion = presentacion;
		this.preciocompra = preciocompra;
		this.proveedor = proveedor;
		this.stock = stock;
	}

	public Producto() {
		super();
		// TODO Auto-generated constructor stub
	}

	

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getDescripcion() {
		return descripcion;
	}

	public void setDescripcion(String descripcion) {
		this.descripcion = descripcion;
	}

	public String getPresentacion() {
		return presentacion;
	}

	public void setPresentacion(String presentacion) {
		this.presentacion = presentacion;
	}

	public double getPreciocompra() {
		return preciocompra;
	}

	public void setPreciocompra(double preciocompra) {
		this.preciocompra = preciocompra;
	}

	public String getProveedor() {
		return proveedor;
	}

	public void setProveedor(String proveedor) {
		this.proveedor = proveedor;
	}

	public double getStock() {
		return stock;
	}

	public void setStock(double stock) {
		this.stock = stock;
	}

	@Override
	public String toString() {
		return "Producto [id=" + id + ", descripcion=" + descripcion + ", presentacion=" + presentacion
				+ ", preciocompra=" + preciocompra + ", proveedor=" + proveedor + "]";
	}
	
	

}
