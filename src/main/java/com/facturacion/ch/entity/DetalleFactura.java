package com.facturacion.ch.entity;

import jakarta.persistence.Column;
import jakarta.persistence.Entity;
import jakarta.persistence.GeneratedValue;
import jakarta.persistence.GenerationType;
import jakarta.persistence.Id;
import jakarta.persistence.JoinColumn;
import jakarta.persistence.ManyToOne;
import jakarta.persistence.Table;

@Entity
@Table(name = "DETALLEFACTURA")
public class DetalleFactura {
	
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private int id;
	
	//@ManyToOne(targetEntity = Factura.class)
	@JoinColumn(name="FACTURA_ID")
	private int factura_id;
	
	//@ManyToOne(targetEntity = Producto.class)
	@JoinColumn(name="PRODUCTO_ID")
	private int producto_id;
	
	@Column(name="CATIDAD")
	private int cantidad;
	
	@Column(name="PRECIOUNITARIO")
	private Double preciounitario;
	
	@Column(name="DESCUENTO")
	private Double descuento;
	
	@Column(name="PRECIOTOTAL")
	private Double preciototal;
	
	@Column(name="ESTADO")
	private Double estado;

	public DetalleFactura(int id, int factura_id, int producto_id, int cantidad, Double preciounitario,
			Double descuento, Double preciototal, Double estado) {
		super();
		this.id = id;
		this.factura_id = factura_id;
		this.producto_id = producto_id;
		this.cantidad = cantidad;
		this.preciounitario = preciounitario;
		this.descuento = descuento;
		this.preciototal = preciototal;
		this.estado = estado;
	}

	@Override
	public String toString() {
		return "DetalleFactura [id=" + id + ", factura_id=" + factura_id + ", producto_id=" + producto_id
				+ ", cantidad=" + cantidad + ", preciounitario=" + preciounitario + ", descuento=" + descuento
				+ ", preciototal=" + preciototal + ", estado=" + estado + "]";
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public int getFactura_id() {
		return factura_id;
	}

	public void setFactura_id(int factura_id) {
		this.factura_id = factura_id;
	}

	public int getProducto_id() {
		return producto_id;
	}

	public void setProducto_id(int producto_id) {
		this.producto_id = producto_id;
	}

	public int getCantidad() {
		return cantidad;
	}

	public void setCantidad(int cantidad) {
		this.cantidad = cantidad;
	}

	public Double getPreciounitario() {
		return preciounitario;
	}

	public void setPreciounitario(Double preciounitario) {
		this.preciounitario = preciounitario;
	}

	public Double getDescuento() {
		return descuento;
	}

	public void setDescuento(Double descuento) {
		this.descuento = descuento;
	}

	public Double getPreciototal() {
		return preciototal;
	}

	public void setPreciototal(Double preciototal) {
		this.preciototal = preciototal;
	}

	public Double getEstado() {
		return estado;
	}

	public void setEstado(Double estado) {
		this.estado = estado;
	}

	public DetalleFactura() {
		super();
		// TODO Auto-generated constructor stub
	}
	
	

}
